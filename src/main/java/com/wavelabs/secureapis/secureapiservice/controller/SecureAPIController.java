package com.wavelabs.secureapis.secureapiservice.controller;

import java.util.Enumeration;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.wavelabs.secureapis.secureapiservice.common.Constants;
import com.wavelabs.secureapis.secureapiservice.model.Mail;
import com.wavelabs.secureapis.secureapiservice.response.Response;
import com.wavelabs.secureapis.secureapiservice.service.EmailService;
import com.wavelabs.secureapis.secureapiservice.service.GetFeedbackQuestionsService;
import com.wavelabs.secureapis.secureapiservice.service.PostFeedbackService;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin("*")
public class SecureAPIController {
	@Autowired
	private EmailService emailService;
	@Autowired
	private PostFeedbackService postingFeedbackService;
	@Autowired
	private GetFeedbackQuestionsService getQuestions;
	@Autowired
	private TemplateEngine templateEngine;

	private static final Logger logger = Logger.getLogger(SecureAPIController.class);

	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Response.class, message = "Mail Sent successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@GetMapping(value = "/sendMail/{toMail}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response sendEmail(@PathVariable String toMail) throws MessagingException {
		logger.info("to mail in api: " + toMail);
		sendmail(toMail);
		return new Response(Constants.MAIL_SENT_SUCCESSFULLY + toMail);
	}

	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Response.class, message = "Mail Sent successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@GetMapping(value = "/postFeedback/{rating}", produces = MediaType.APPLICATION_JSON_VALUE)
	public String postFeedbackew(@PathVariable String rating) {
		Context context = new Context();
		logger.info("API call.....! " + rating);
		return templateEngine.process("Result", context);
	}

	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Response.class, message = "Send Feedback Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@PostMapping(value = "/postFeedbackNew", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String postFeedbackNew(HttpServletRequest request) {
		Enumeration<String> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String paramter = params.nextElement();
			logger.info("in post api call:  " + paramter + " Paramter value: " + request.getParameter(paramter));
		}
		String response = postingFeedbackService.postingFeedack(request);
		logger.info("response:::" + response);
		Context context = new Context();
		return templateEngine.process("Result", context);
	}

	@GetMapping("/getQuestions")
	public String getFeedback() {
		getQuestions.getFeedbackQuestions();
		return null;
	}

	public void sendmail(String tomail) throws MessagingException {
		logger.info("in sendMail " + tomail);
		Mail mail = new Mail();
		mail.setFrom("sarupajanney@gmail.com");
		mail.setTo(tomail);
		mail.setSubject("Sending Email with Attachment USing My Email Service");
		mail.setContent("Email with Attachement using spring boot application");
		emailService.sendSimpleMessage(mail);
	}
}
