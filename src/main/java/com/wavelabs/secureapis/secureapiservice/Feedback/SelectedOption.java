package com.wavelabs.secureapis.secureapiservice.Feedback;

public class SelectedOption {

	private Boolean deleted;
	private String id;
	private Integer order;
	private String text;

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "SelectedOption [deleted=" + deleted + ", id=" + id + ", order=" + order + ", text=" + text + "]";
	}

	
}