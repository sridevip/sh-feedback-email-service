package com.wavelabs.secureapis.secureapiservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {

    private TemplateEngine templateEngine;

    @Autowired
    public MailContentBuilder(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
    }

    public String generateMailContent() {
        Context context = new Context();
        context.setVariable("question", "Question 1 Like app.?");
        return templateEngine.process("Feedback", context);
    }
}