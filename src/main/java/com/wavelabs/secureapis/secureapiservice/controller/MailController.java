package com.wavelabs.secureapis.secureapiservice.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.secureapis.secureapiservice.common.Constants;
import com.wavelabs.secureapis.secureapiservice.questions.Question;
import com.wavelabs.secureapis.secureapiservice.response.Response;
import com.wavelabs.secureapis.secureapiservice.service.GetFeedbackQuestionsService;
import com.wavelabs.secureapis.secureapiservice.service.NewEmailService;

import freemarker.template.TemplateException;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin("*")
public class MailController {

	@Autowired
	private NewEmailService emailService;
	@Autowired
	private GetFeedbackQuestionsService feedbackQuestions;

	private static final Logger logger = Logger.getLogger(MailController.class);

	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = Response.class, message = "Mail Sent successfully"),
			@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, response = String.class, message = "Invalid parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })
	@GetMapping(value = "/sendHtml/{toMail}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Response createHtmlMail(Model model, @PathVariable String toMail)
			throws IOException, MessagingException, TemplateException {
		Map<String, Object> templateModel = new HashMap<>();
		templateModel.put("recipientName", toMail);
		templateModel.put("text", "xyz");
		templateModel.put("senderName", "xyz@gmail.com");
		List<Question> questions = feedbackQuestions.getFeedbackQuestions().getQuestions();
		logger.info("questions::" + questions);
		emailService.sendMessageUsingThymeleafTemplate("xyz@gmail.com", "xyz", templateModel);
		return new Response(Constants.MAIL_SENT_SUCCESSFULLY + toMail);
	}
}
