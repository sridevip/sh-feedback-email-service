package com.wavelabs.secureapis.secureapiservice.questions;

import java.util.List;

public class FeedbackQuestion {
    private Object id;
    private String appId;
    private String appName;
    private String questionnaireName;
    private List<Question> questions;
    private String groupTag;
    private String groupName;
    private Object eventTag;
    private Object eventName;
    private String status;
    private boolean userIdFlag;
    private String createdBy;
    
    
	public FeedbackQuestion() {
		super();
	}
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getQuestionnaireName() {
		return questionnaireName;
	}
	public void setQuestionnaireName(String questionnaireName) {
		this.questionnaireName = questionnaireName;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public String getGroupTag() {
		return groupTag;
	}
	public void setGroupTag(String groupTag) {
		this.groupTag = groupTag;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Object getEventTag() {
		return eventTag;
	}
	public void setEventTag(Object eventTag) {
		this.eventTag = eventTag;
	}
	public Object getEventName() {
		return eventName;
	}
	public void setEventName(Object eventName) {
		this.eventName = eventName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isUserIdFlag() {
		return userIdFlag;
	}
	public void setUserIdFlag(boolean userIdFlag) {
		this.userIdFlag = userIdFlag;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Override
	public String toString() {
		return "FeedbackQuestion [id=" + id + ", appId=" + appId + ", appName=" + appName + ", questionnaireName="
				+ questionnaireName + ", questions=" + questions + ", groupTag=" + groupTag + ", groupName=" + groupName
				+ ", eventTag=" + eventTag + ", eventName=" + eventName + ", status=" + status + ", userIdFlag="
				+ userIdFlag + ", createdBy=" + createdBy + "]";
	}
	
	
}