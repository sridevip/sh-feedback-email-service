package com.wavelabs.secureapis.secureapiservice.questions;

import java.util.List;

import com.wavelabs.secureapis.secureapiservice.model.OptionObj;

public class FeedbackQuestions {

	private String questionType;
	private String question;
	private List<OptionObj> options;
	private String questionId;

	public FeedbackQuestions(String questionType, String question, List<OptionObj> options, String questionId) {
		super();
		this.questionType = questionType;
		this.question = question;
		this.options = options;
		this.questionId = questionId;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<OptionObj> getOptions() {
		return options;
	}

	public void setOptions(List<OptionObj> options) {
		this.options = options;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

}
